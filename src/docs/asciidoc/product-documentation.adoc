= NodeJS Gradle Plugin
:author: Schalk W. Cronjé
:doctype: book
:toc: right
:icons: font
:source-highlighter: rouge
include::attributes.adoc[]

include::parts/introduction.adoc[]

include::parts/alternatives.adoc[]

include::parts/bootstrap.adoc[]

= Working with Node

include::parts/configure-node-defaults.adoc[]

include::parts/platform-installation-support.adoc[]

= Working with NPM

include::parts/configure-npm-defaults.adoc[]

include::parts/npmtask.adoc[]

include::parts/package-json.adoc[]

include::parts/dependency-management.adoc[]

= Working with Gulp

include::parts/gulp.adoc[]

= Command-line operations

include::parts/running-from-cmdline.adoc[]

= Developing with Node

include::parts/developing-with-node.adoc[]

= Advanced Topics

include::parts/running-js-tools.adoc[]

include::parts/wrapping-npm-package.adoc[]

include::parts/tips-and-tricks.adoc[]

