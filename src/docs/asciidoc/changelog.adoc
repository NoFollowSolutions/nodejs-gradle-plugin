= Changelog & Release Notes {revnumber}

include::{gradle-projectdir}/CHANGELOG.adoc[tags=changelog,leveloffset=-1]
