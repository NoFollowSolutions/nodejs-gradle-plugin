/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.internal.npm

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.ysb33r.gradle.nodejs.NodeJSExtension
import org.ysb33r.gradle.nodejs.NpmExecSpec
import org.ysb33r.gradle.nodejs.NpmExtension
import org.ysb33r.grolifant.api.core.ProjectOperations

import static org.ysb33r.gradle.nodejs.utils.npm.NpmExecutor.configureSpecFromExtensions

/** Creates instances for {@link NpmExecSpec}
 *
 * @since 0.1
 */
@CompileStatic
class NpmExecSpecInstantiator {

    NpmExecSpecInstantiator(NodeJSExtension nodejs, NpmExtension npmExtension) {
        this.npmExtension = npmExtension
        this.nodejs = nodejs
    }

    /** Instantiates a NPM execution specification.
     *
     * @param project Project that this execution specification will be associated with.
     * @return New NPM execution specification.
     * @deprecated
     */
    @Deprecated
    NpmExecSpec create(Project project) {
        create(ProjectOperations.find(project))
    }

    /**
     * Instantiates an execution specification in a configuration cache safe way.
     *
     * @param projectOperations {@link ProjectOperations} instance for the project that this execution specification
     *                                                   will be associated with.
     * @return New execution specification.
     *
     * @since 0.10
     */
    NpmExecSpec create(ProjectOperations projectOperations) {
        NpmExecSpec execSpec = new NpmExecSpec(projectOperations)
        configureSpecFromExtensions(execSpec, nodejs, npmExtension)
        execSpec
    }

    private final NpmExtension npmExtension
    private final NodeJSExtension nodejs
}
