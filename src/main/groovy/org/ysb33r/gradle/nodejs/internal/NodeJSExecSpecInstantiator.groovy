/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.internal

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.ysb33r.gradle.nodejs.NodeJSExecSpec
import org.ysb33r.gradle.nodejs.NodeJSExtension
import org.ysb33r.gradle.nodejs.utils.NodeJSExecutor
import org.ysb33r.grolifant.api.core.ProjectOperations

/** Creates instances for {@link NodeJSExecSpec}
 *
 * @since 0.1
 */
@CompileStatic
class NodeJSExecSpecInstantiator {

    /**
     *
     * @param nodejs
     *
     * @since 0.10
     */
    NodeJSExecSpecInstantiator(NodeJSExtension nodejs, ProjectOperations projectOperations) {
        this.nodejs = nodejs
        this.projectOperations = projectOperations
    }

    /** Instantiates a NPM execution specification.
     *
     * @param project Project that this execution specification will be associated with.
     * @return New NPM execution specification.
     *
     * @deprecated
     */
    @Deprecated
    @SuppressWarnings('UnusedMethodParameter')
    NodeJSExecSpec create(Project project) {
        create()
    }

    /**
     * Instantiates an execution specification in a configuration cache safe way.
     *
     * @param projectOperations {@link ProjectOperations} instance for the project that this execution specification
     *                                                   will be associated with.
     * @return New execution specification.
     *
     * @since 0.10
     */
    NodeJSExecSpec create() {
        NodeJSExecSpec execSpec = new NodeJSExecSpec(projectOperations)
        execSpec.environment = NodeJSExecutor.defaultEnvironment
        execSpec
    }

    private final NodeJSExtension nodejs
    private final ProjectOperations projectOperations
}
