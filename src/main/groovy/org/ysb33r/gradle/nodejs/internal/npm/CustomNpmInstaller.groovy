/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.internal.npm

import groovy.transform.CompileStatic
import org.gradle.api.provider.Provider
import org.ysb33r.gradle.nodejs.NodeJSExtension
import org.ysb33r.gradle.nodejs.NpmExtension
import org.ysb33r.gradle.nodejs.SimpleNpmPackageDescriptor
import org.ysb33r.gradle.nodejs.errors.NpmPackageResolveException
import org.ysb33r.gradle.nodejs.utils.npm.NpmExecutor
import org.ysb33r.grolifant.api.core.ProjectOperations
import org.ysb33r.grolifant.api.v4.runnable.ExecutableDownloader

import static org.ysb33r.gradle.nodejs.NpmDependencyGroup.DEVELOPMENT
import static org.ysb33r.gradle.nodejs.utils.npm.NpmExecutor.FORCE_NODE_DEFAULT_FOR_NPM

/**
 * Installs a custom version of NPM in node_modules.
 *
 * @since 0.12
 */
@CompileStatic
class CustomNpmInstaller implements ExecutableDownloader {

    /**
     *
     * @param nodeJSExtension {@code nodejs} extension.
     *
     */
    CustomNpmInstaller(
        ProjectOperations projectOperations,
        NodeJSExtension nodeJSExtension,
        NpmExtension npmExtension
    ) {
        this.executor = new NpmExecutor(projectOperations, nodeJSExtension, npmExtension, FORCE_NODE_DEFAULT_FOR_NPM)
        this.npmHome = npmExtension.homeDirectoryProvider
    }

    /**
     * Locates, and optionally downloads, NPM by version.
     *
     * @param version version to download.
     *
     * @return Location of NPM executable.
     */
    @Override
    File getByVersion(String version) {
        File expected = expectedLocationFor()
        if (expected.exists()) {
            File checkVersion = new File(expected.parentFile.parentFile, 'docs/output/commands/npm.html')
            if (checkVersion.exists() && checkVersion.text.contains(version)) {
                return expected
            }
        }
        install(version)
    }

    @SuppressWarnings('UnnecessaryCast')
    File install(String version) {
        executor.installNpmPackage(
            new SimpleNpmPackageDescriptor(null, 'npm', version),
            DEVELOPMENT,
            [] as List<String>
        )

        File expected = expectedLocationFor()

        if (expected.exists()) {
            expected
        } else {
            throw new NpmPackageResolveException("Could not install NPM version '${version}'")
        }
    }

    private File expectedLocationFor() {
        new File(npmHome.get(), 'node_modules/npm/bin/npm-cli.js').absoluteFile
    }

    private final NpmExecutor executor
    private final Provider<File> npmHome
}
