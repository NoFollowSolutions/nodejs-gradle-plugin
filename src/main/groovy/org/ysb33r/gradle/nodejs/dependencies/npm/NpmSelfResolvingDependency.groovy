/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.dependencies.npm

import groovy.transform.CompileStatic
import groovy.transform.PackageScope
import groovy.util.logging.Slf4j
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.artifacts.Dependency
import org.gradle.api.artifacts.FileCollectionDependency
import org.gradle.api.artifacts.component.ComponentIdentifier
import org.gradle.api.file.FileCollection
import org.gradle.api.file.FileTree
import org.gradle.api.internal.artifacts.dependencies.SelfResolvingDependencyInternal
import org.gradle.api.internal.file.FileSystemSubset
import org.gradle.api.tasks.TaskDependency
import org.ysb33r.gradle.nodejs.NodeJSExtension
import org.ysb33r.gradle.nodejs.NpmDependency
import org.ysb33r.gradle.nodejs.NpmExtension
import org.ysb33r.gradle.nodejs.internal.npm.NpmComponentIdentifier
import org.ysb33r.gradle.nodejs.utils.npm.NpmExecutor
import org.ysb33r.grolifant.api.core.OperatingSystem
import org.ysb33r.grolifant.api.core.ProjectOperations

import javax.annotation.Nullable

import static org.ysb33r.grolifant.api.v4.StringUtils.stringize

/** An NPM dependency that can resolve itself.
 *
 * @since 0.5
 */
@CompileStatic
@Slf4j
class NpmSelfResolvingDependency extends NpmDependency implements FileCollectionDependency,
    SelfResolvingDependencyInternal {

    /** Allows additional parameters to be set
     *
     * @param project
     * @param properties
     */
    NpmSelfResolvingDependency(Project project, final Map<String, ?> properties) {
        super(onlyNpmDependencyKeys(properties))
        this.projectOperations = ProjectOperations.find(project)
        this.nodeJSExtension = project.extensions.getByType(NodeJSExtension)
        this.npmExtension = project.extensions.getByType(NpmExtension)
        this.npmExecutor = new NpmExecutor(projectOperations, nodeJSExtension, npmExtension)
        this.installArgs = extractInstallArgs(properties)
        this.path = properties[PATH]
    }

    /**
     *
     * @param project
     * @param nodeJSExtension
     * @param npmExtension
     * @param properties Various {@link NpmDependency} peroperties, but also recognises {@code install-args},
     *   which is a list of additional arguments that can be passed to NPM during package installation.
     *
     * @deprecated
     */
    @Deprecated
    NpmSelfResolvingDependency(
        Project project,
        NodeJSExtension nodeJSExtension,
        NpmExtension npmExtension,
        final Map<String, Object> properties
    ) {
        super(onlyNpmDependencyKeys(properties))
        this.projectOperations = ProjectOperations.find(project)
        this.nodeJSExtension = nodeJSExtension
        this.npmExtension = npmExtension
        this.npmExecutor = new NpmExecutor(projectOperations, nodeJSExtension, npmExtension)
        this.installArgs.addAll(extractInstallArgs(properties))
        this.path = properties[PATH]
    }

    /**
     *
     * @param projectOperations
     * @param nodeJSExtension
     * @param npmExtension
     * @param properties Various {@link NpmDependency} peroperties, but also recognises {@code install-args},
     *   which is a list of additional arguments that can be passed to NPM during package installation.
     *
     * @since 0.10
     */
    NpmSelfResolvingDependency(
        ProjectOperations projectOperations,
        NodeJSExtension nodeJSExtension,
        NpmExtension npmExtension,
        final Map<String, Object> properties
    ) {
        super(onlyNpmDependencyKeys(properties))
        this.projectOperations = projectOperations
        this.nodeJSExtension = nodeJSExtension
        this.npmExtension = npmExtension
        this.npmExecutor = new NpmExecutor(projectOperations, nodeJSExtension, npmExtension)
        this.installArgs.addAll(extractInstallArgs(properties))
        this.path = properties[PATH]
    }

    @Override
    String getGroup() {
        this.scope
    }

    @Override
    String getName() {
        this.packageName
    }

    @Override
    String getVersion() {
        this.tagName == '+' ? 'latest' : this.tagName
    }

    @Override
    boolean contentEquals(Dependency dependency) {
        dependency instanceof NpmSelfResolvingDependency &&
            ((NpmSelfResolvingDependency) dependency).packageName == packageName &&
            ((NpmSelfResolvingDependency) dependency).tagName == tagName &&
            ((NpmSelfResolvingDependency) dependency).scope == scope
    }

    @Override
    @SuppressWarnings('UnnecessaryCast')
    Dependency copy() {
        Map<String, Object> pkg = [name: packageName, tag: tagName, type: installGroup] as Map<String, Object>
        if (scope) {
            pkg.put('scope', scope)
        }
        if (!installArgs.empty) {
            pkg.put(INSTALL_ARGS, installArgs)
        }
        if (path) {
            pkg.put(PATH, path)
        }
        new NpmSelfResolvingDependency(
            projectOperations,
            nodeJSExtension,
            npmExtension,
            (Map<String, Object>) pkg
        )
    }

    @Override
    String getReason() {
        this.reason
    }

    @Override
    void because(@Nullable String s) {
        this.reason = s
    }

    @Override
    Set<File> resolve() {
        Map<String, Object> env = [:]

        if (path) {
            env[OperatingSystem.current().pathVar] = path
        }

        npmExecutor.installNpmPackage(this, installGroup, installArgs, env).files
    }

    @Override
    Set<File> resolve(boolean b) {
        log.warn('Ignoring transitive parameter for NPM')
        resolve()
    }

    final TaskDependency buildDependencies = NO_TASK_DEPENDENCIES

    @Override
    FileCollection getFiles() {
        FileTree fileTree = projectOperations.fileTree(npmExecutor.getPackageInstallationFolder(this))

        if (fileTree.empty) {
            resolve()
        }

        fileTree
    }

    /** @deprecated */
    // @Override
    @SuppressWarnings('UnusedMethodParameter')
    void registerWatchPoints(FileSystemSubset.Builder builder) {
        log.warn 'Ignoring watch point registry'
    }

    @Override
    ComponentIdentifier getTargetComponentId() {
        new NpmComponentIdentifier("NPM: ${this.toString()}")
    }

    @PackageScope
    String getPath() { this.path }

    @PackageScope
    List<String> getInstallArgs() { this.installArgs }

    private static class NoTaskDependencies implements TaskDependency {
        static final Set EMPTY_SET = []

        @Override
        Set<? extends Task> getDependencies(Task task) {
            EMPTY_SET
        }
    }

    private static List<String> extractInstallArgs(Map<String, Object> userMap) {
        stringize((userMap[INSTALL_ARGS] ?: []) as List)
    }

    private static final NoTaskDependencies NO_TASK_DEPENDENCIES = new NoTaskDependencies()
    private final static String PATH = 'path'
    private final static String INSTALL_ARGS = 'install-args'
    private String reason
    private final ProjectOperations projectOperations
    private final NodeJSExtension nodeJSExtension
    private final NpmExtension npmExtension
    private final NpmExecutor npmExecutor
    private final List<String> installArgs = []
    private final String path
}
