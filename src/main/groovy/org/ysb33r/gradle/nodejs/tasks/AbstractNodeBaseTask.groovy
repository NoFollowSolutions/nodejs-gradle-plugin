/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.tasks

import groovy.transform.CompileStatic
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Internal
import org.gradle.process.ExecResult
import org.ysb33r.gradle.nodejs.NodeJSExecSpec
import org.ysb33r.gradle.nodejs.NodeJSExtension
import org.ysb33r.gradle.nodejs.NpmExtension
import org.ysb33r.gradle.nodejs.internal.NodeJSExecSpecInstantiator
import org.ysb33r.gradle.nodejs.utils.NodeJSExecutor
import org.ysb33r.gradle.nodejs.utils.npm.NpmExecutor
import org.ysb33r.grolifant.api.core.ProjectOperations

/** A base class that will provide NodeJS and NPM task extensions.
 *
 * @since 0.1
 */
@CompileStatic
class AbstractNodeBaseTask extends DefaultTask {

    protected AbstractNodeBaseTask() {
        super()
        npmExtension = (NpmExtension) (extensions.create(NpmExtension.NAME, NpmExtension, this))
        nodeExtension = (NodeJSExtension) (extensions.create(NodeJSExtension.NAME, NodeJSExtension, this))
        projectOperations = ProjectOperations.find(project)
        npmExecutor = new NpmExecutor(projectOperations, nodeExtension, npmExtension)
        instantiator = new NodeJSExecSpecInstantiator(nodeExtension, projectOperations)
    }

    void environment(Map<String, ?> env) {
        nodeExtension.environment(env)
    }

    void setEnvironment(Map<String, ?> env) {
        nodeExtension.environment = env
    }

    @Input
    Map<String, Object> getEnvironment() {
        nodeExtension.environment as Map<String, Object>
    }

    /** Internal access to attached NPM extension.
     *
     * @return NPM extension.
     */
    @Internal
    protected NpmExtension getNpmExtension() {
        this.npmExtension
    }

    /** Internal access to attached NodeJS extension.
     *
     * @return NodeJS extension.
     */
    @Internal
    protected NodeJSExtension getNodeExtension() {
        this.nodeExtension
    }

    @Internal
    protected ProjectOperations getProjectOperations() {
        this.projectOperations
    }

    @Internal
    protected NpmExecutor getNpmExecutor() {
        this.npmExecutor
    }

    /** Creates a node.js execution specification and populates it with default
     * working directory, environment and location of {@code node}.
     *
     * @return Pre-configured execution specification.
     */
    protected NodeJSExecSpec createExecSpec() {
        NodeJSExecSpec execSpec = instantiator.create()
        NodeJSExecutor.configureSpecFromExtensions(execSpec, nodeExtension)
        execSpec.workingDir(npmExtension.homeDirectory)
        execSpec.environment = environment
        execSpec
    }

    /** Executes a configured execution specification.
     *
     * @param execSpec Configured specification.
     * @return Execution result
     */
    protected ExecResult runExecSpec(NodeJSExecSpec execSpec) {
        nodeExtension.exec(execSpec)
    }

    private final NodeJSExecSpecInstantiator instantiator
    private final NpmExtension npmExtension
    private final NodeJSExtension nodeExtension
    private final ProjectOperations projectOperations
    private final NpmExecutor npmExecutor
}
