/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.pkgwrapper

import groovy.transform.CompileStatic
import groovy.transform.InheritConstructors
import org.gradle.api.provider.Provider
import org.ysb33r.gradle.nodejs.errors.UnsupportedMethodException
import org.ysb33r.gradle.nodejs.internal.npm.TagBasedNpmPackageResolver
import org.ysb33r.grolifant.api.v4.exec.ResolvableExecutable
import org.ysb33r.grolifant.api.v4.exec.ResolverFactoryRegistry

/** A base class for creating resolvers for specific NPM packages.
 *
 * <p> This class is meant to be used when creating an ecosystem for specific broad-use
 * NPM-based tools such as Gulp.
 *
 * @since 0.2*
 * @deprecated DO not use this class directly. Just use {@link AbstractPackageWrappingExtension}
 */
@CompileStatic
@InheritConstructors
@Deprecated
class TagBasedPackageResolver extends TagBasedNpmPackageResolver {

    /** Obtains the installation group for a package
     *
     * @deprecated Use{@link PackageInstallGroup}.
     */
    @Deprecated
    static interface InstallGroup extends PackageInstallGroup {
    }

    /** Obtains the entry point for package.
     *
     * @deprecated Use{@link PackageEntryPoint}
     */
    @Deprecated
    static interface EntryPoint extends PackageEntryPoint {
    }

    /** Returns a name that can be used as a key into a {@link ResolverFactoryRegistry}.
     *
     * Always return 'version'.
     *
     * @deprecated
     */
    @Deprecated
    final String name = 'version'

    @SuppressWarnings('DuplicateStringLiteral')
    ResolvableExecutable build(Map<String, Object> options, Object from) {
        Provider<File> provider
        if (options.containsKey('version')) {
            provider = super.resolverFromVersion(from)
        } else if (options.containsKey('path')) {
            provider = super.resolverFromPath(from)
        } else {
            throw new UnsupportedMethodException("${options} are not supported in this deprecated method")
        }
        new ResolvableExecutable() {
            @Override
            File getExecutable() {
                provider.get()
            }
        }
    }
}
