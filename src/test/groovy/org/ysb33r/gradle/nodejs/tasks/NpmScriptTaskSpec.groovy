/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2021
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.tasks

import org.ysb33r.gradle.nodejs.helper.UnittestBaseSpecification

class NpmScriptTaskSpec extends UnittestBaseSpecification {

    void 'Environment must be able to evaluate providers'() {
        setup:
        project.apply plugin: 'org.ysb33r.nodejs.dev'
        def iAmBar = project.provider { -> 'bar' }

        when: 'A task is created'
        def task = project.tasks.create('myNpmTask', NpmScriptTask)
        task.scriptName = 'looney.tunes'

        and: 'A provider is set on the environment'
        task.environment 'foo': iAmBar

        then: 'The provider must be evaluated to a string value'
        task.environment['foo'].get() == 'bar'
    }

}