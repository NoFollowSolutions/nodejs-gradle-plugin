= Changelog
:issue: https://github.com/ysb33r/nodejs-gradle-plugin/issues/

== v0.12.0 - 0.12.1

// tag::changelog[]
== Features

* {issue}35[#35] - Ability to use a custom NPM.
* {issue}37[#37] - When `idea` plugin is combined with `org.ysb33r.nodejs.wrapper` plugin, node & npm settings are updated in Intellij IDEA.
* {issue}39[#39] - Consolidate `.npmrc` locations and provide additional configuration possibilities.

== Bugs

* {issue}40[#40] - `NpmExecSpec` must set location of `npm`.
* {issue}41[#41] - `NpmExecSpec` must set honour local & global configuration file settings as determined by the associated `NpmExtension`.

// end::changelog[]

== v0.10.0

=== Features

* {issue}31[#31] - Configuration-cache enabled.
* {issue}32[#32] - Deprecated `project.nodeexec` & `project.npmexec` and replaced with `node.exec` and `npm.npmexec` instead.

=== Bugs

* {issue}29[#29] - Evaluate environment settings where values contain providers.

=== Other

* {issue}33[#33] - `dependencies.npmPackage` is now handled as a service rather than extra properties.

=== Deprecations

* Where classes have both constructors taking `Project` or `ProjectOperations`, then constructor taking `Project` has been deprecated.
* All methods on extensions starting with `getResolvable` have been replaced. Use either `getExecutable` or `getNpmCliJsProvider`.
* `executable version/path/search` have been replaced by `executableByVersion / executableByPath / executableBySearchPath`.

=== Breaking changes

* Minimum supported Gradle version is now 4.9.
* `project.nodeexec` & `project.npmexec` - use `node.exec` and `npm.exec` instead.
* Setting a different version for NPM has never been implemented properly and has now been removed. Please raise an issue if you need this support.
* `getResolverFactoryRegistry` has been removed from `AbstractPackageWrappingExtension`. There is no longer a need for this.
* Maven coordinates are now `org.ysb33r.gradle` and not `gradle.plugin.org.ysb33r.gradle`.

== v0.9.0 / v0.9.1 / v0.9.2 / v0.9.3 / v0.9.4

=== Features

* {issue}15[#15] - Support for running `npx`
* {issue}16[#16] - Run `node`, `npm` and `npx` from the command-line via Gradle.
* {issue}20[#20] - Provide conventions for developing in Nodejs
* {issue}21[#21] - Wrappers for `node`, `npm`, `npx`.

=== Bugs

* {issue}22[#22] - Wrappers should resolve relative paths correctly
* {issue}23[#23] - Maintain spacing and order of package.json when updating.
* {issue}24[#24] - npmRun tasks are appending `--scripts-prepend-node-path=true` which causes scripts not to execute.
* {issue}25[#25] - Location of npx/npm should be of system path when running node.
* {issue}28[#28] - Tasks `npmRun*`, `npm` and `npx` are not placing node on the path. (Please do `rm -rf ~/.gradle/native-binaries/nodejs` if you  were affected by this bug)


=== Breaking changes

* Minimum supported Gradle version is now 4.7.

== v0.8.0

=== Features

* {issue}14[#14] - `packageJsonInstall` task.

=== Breaking changes

* Minimum supported Gradle version is now 4.3

=== Other

* {issue}17[#17] - Upgraded to Grolifant 0.17.0 and removed deprecated usages.
* NpmExtension has providers

== v0.7.1

=== Features

* {issue}13[#13] - Allow every task to have it's own nodejs & npm extension.

=== Other

* Fix documentation generation that was broken in 0.7.0.

== v0.6.2

=== Features

* {issue}11[#11] - Environment can be modified on a per NPM package installation basis.

=== Bugs

* {issue}12[#12] - Paths are not copied when NpmSelfResolvingDependency is cloned.

== v0.5

=== Features

* {issue}7[#7] - `NpmExecutor` utilities are in public API.
* {issue}8[#8] - `NpmSelfResolvingDependency` is in public API.
* {issue}9[#9] - `NodeJSExecutor` utilities are in public API.

== v0.4

=== Bugs

* Some NPM packages were resolving to Gulp.
* Names of extensions are resolved correctly

== v0.3

=== Features

* {issue}1[#1] - Improve caching of `NpmPackageJsonInstall` task type.
* {issue}2[#2] - Map Node/NPM/Gulp logging levels to Gradle levels.
* Gradle 5.x compatible.

=== Breaking changes

* No longer compatible with Gradle < 4.0.
* Requires at least JDK8.

=== Other

* Upgraded to Grolifant 0.10

== v0.2

* {issue}5[#5] - Move project from Schalk Cronjé's personal repository on GitHub to *Ysb33r Software Foundation* repository on GitLab.
* Upgrade to Grolifant 0.5 and refactor local codebase.

== v0.1

First release:

* Download Node.js on supported platforms
* NPM support
* Gulp support

